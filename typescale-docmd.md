---
layout: demo-docmd
---

<br>

# Documentation Markdown typescale

The following demonstrates how different elements are styled and behave responsively.

**Namely:**

- Headings are scaled proportionately down at smaller breakpoints to consider the reading experience and prevent longer terms from breaking the layout and causing the entire page to scroll horizontally.
- Tables scroll horizontally at smaller breakpoints to prevent them from breaking the layout and causing the entire page to scroll horizontally.

Resize the browser window to see the impact on headings and tables.

---

# Heading 1

## Heading 2

### Heading 3

#### Heading 4

##### Heading 5

###### Heading 6

---

# Heading 1

The system requirements include details on the minimum hardware, software, database, and additional requirements to support GitLab. Omnibus GitLab: The official deb/rpm packages that contain a bundle of GitLab and the various components it depends on, like PostgreSQL, Redis, Sidekiq, etc.

## Heading 2

The system requirements include details on the minimum hardware, software, database, and additional requirements to support GitLab. Omnibus GitLab: The official deb/rpm packages that contain a bundle of GitLab and the various components it depends on, like PostgreSQL, Redis, Sidekiq, etc.

### Heading 3

The system requirements include details on the minimum hardware, software, database, and additional requirements to support GitLab. Omnibus GitLab: The official deb/rpm packages that contain a bundle of GitLab and the various components it depends on, like PostgreSQL, Redis, Sidekiq, etc.

#### Heading 4

The system requirements include details on the minimum hardware, software, database, and additional requirements to support GitLab. Omnibus GitLab: The official deb/rpm packages that contain a bundle of GitLab and the various components it depends on, like PostgreSQL, Redis, Sidekiq, etc.

##### Heading 5

The system requirements include details on the minimum hardware, software, database, and additional requirements to support GitLab. Omnibus GitLab: The official deb/rpm packages that contain a bundle of GitLab and the various components it depends on, like PostgreSQL, Redis, Sidekiq, etc.

###### Heading 6

The system requirements include details on the minimum hardware, software, database, and additional requirements to support GitLab. Omnibus GitLab: The official deb/rpm packages that contain a bundle of GitLab and the various components it depends on, like PostgreSQL, Redis, Sidekiq, etc.

---

- Beginning in GitLab 12.9, we only support node.js 10.13.0 or higher, and we have dropped support for node.js 8. (node.js 6 support was dropped in GitLab 11.8).
- Deploy tokens belong to either a project or a group.
- You can filter the list of MRs by milestone and label.
- Issues are the fundamental medium for collaborating on ideas and planning work in GitLab.
- Every Release has a description.
  - The minimum required Go version is 1.13.
  - For an overview of the directory structure, read the structure documentation.
- Omnibus GitLab: The official deb/rpm packages that contain a bundle of GitLab and the various components it depends on, like PostgreSQL, Redis, Sidekiq, etc.

1. Beginning in GitLab 12.9, we only support node.js 10.13.0 or higher, and we have dropped support for node.js 8. (node.js 6 support was dropped in GitLab 11.8).
1. Deploy tokens belong to either a project or a group.
1. You can filter the list of MRs by milestone and label.
1. Issues are the fundamental medium for collaborating on ideas and planning work in GitLab.
1. Every Release has a description.
   1. The minimum required Go version is 1.13.
   1. For an overview of the directory structure, read the structure documentation.
1. Omnibus GitLab: The official deb/rpm packages that contain a bundle of GitLab and the various components it depends on, like PostgreSQL, Redis, Sidekiq, etc.

[This is a link](#)

**Bold text**

_Emphasized text_

~~Strikethrough~~

Superscript<sup>®</sup>

<kbd>Keyboard input Cmd</kbd>

<cite>Citation</cite>

<mark>Highlighted text</mark>

`Inline code docker: Error response from daemon: failed to copy xattrs`

```
Code block docker: Error response from daemon: failed to copy xattrs
```

<div class="table-responsive" markdown="1">

| Responsive table | Scrolls horizontally | At small breakpoints |
| ------ | ------ | ------ |
| At GitLab, everyone can contribute! Omnibus GitLab: The official deb/rpm packages that contain a bundle of GitLab and the various components it depends on, like PostgreSQL, Redis, Sidekiq, etc. | GitLab’s Releases are a way to track deliverables in your project. `ADDITIONAL_CA_CERT_BUNDLE` | Deploy keys are shareable between projects that are not related or don’t even belong to the same group. |
| Merge requests allow you to visualize and collaborate on the proposed changes to source code that exist as commits on a given Git branch. `CLAIR_DB_CONNECTION_STRING` | When you create a project in GitLab, you’ll have access to a large number of features. The user that would show for README.md would be @user2. | `docker: Error response from daemon: failed to copy xattrs` |
| 8 cores supports up to 1000 users. | GitLab can be installed in most GNU/Linux distributions and in a number of cloud providers. Every Release has a description. | To access the Analytics workspace, click on More > Analytics in the top navigation bar. `$CI_REGISTRY_IMAGE/$CI_COMMIT_REF_SLUG` |

</div>
