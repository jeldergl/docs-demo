---
layout: default
---

<br>

# Exploration

## Concept typescale

The concept typescale seeks to provide sufficient whitespace that increases scannability and allows the reading experience to feel lighter, more modern, and open. Headings are visually connected to their content and are easily scannable. Color is used to enhance the design and make the overall site feel more themed and unified.

- [Styles](typescale.html)
- [Example](example-content.html)

## Documentation Markdown typescale

The Documentation Markdown typescale is part of [GitLab UI](https://gitlab-org.gitlab.io/gitlab-ui/?path=/story/base-markdown--typescale), but it hasn’t been applied to the Docs site. The concept typescale above is based on this and further notes can be found in the [related issue](https://gitlab.com/gitlab-org/gitlab/-/issues/300424#note_498718815).

- [Styles](typescale-docmd.html)
- [Example](example-content-docmd.html)

## Side-by-side

Compare each typescale next to each other.

- [Styles](typescale-sbs.html)
